# expo-deploy-sonar-scanner

Provides everything you need to deploy Expo apps via cloud-based CI (i.e. Bitbucket Pipelines, Travis, CircleCI) as well as analyze the source code with `sonar-scanner` before pushing.

Includes:

- `sonar-scanner`
- `exp`
- `npm`
- `node (version 8.x)`
- `yarn` (which you really should use, or your Jest tests may fail)
