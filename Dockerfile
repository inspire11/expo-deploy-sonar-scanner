FROM openjdk:8-jre-slim

ENV SONAR_SCANNER_VERSION=3.1.0.1141-linux \
    SONAR_SCANNER_HOME=/opt/sonar-scanner

RUN set -x \
  && apt-get -y update \
  && apt-get -y install curl gnupg wget unzip git procps \

  # Fetch and install sonar scanner
  && apt-get -y install nodejs \
  && wget -O sonar-scanner.zip --no-verbose https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-$SONAR_SCANNER_VERSION.zip \
  && unzip sonar-scanner.zip || true \
  && mkdir -p /opt \
  && mv sonar-scanner-$SONAR_SCANNER_VERSION $SONAR_SCANNER_HOME \

  # Add symlinks
  && ln -s $SONAR_SCANNER_HOME/bin/sonar-scanner /usr/bin/sonar-scanner \

  # Remove zip files
  && rm sonar-scanner.zip \

  # Fetch and install node
  && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
  && apt-get -y install nodejs \

  # Install yarn
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update && apt-get -y install yarn \

  # Install global exp
  && npm install -g exp \

  # Install phantomjs
  && wget -O phantomjs.tar.bz2 --no-verbose 'https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2' \ 
  && tar xjf phantomjs.tar.bz2 \
  && mv phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/bin \
  && rm phantomjs.tar.bz2 
